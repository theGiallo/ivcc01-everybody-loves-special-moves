#!/bin/bash
FORMAT=$(echo -e"e %e\nE %E\nS %S\nU %U\nP %P\n%F %R\n%C\n%x")
#echo $FORMAT
echo -e "\n\n################################################################################\nsmall"
time ./Release/IVCC01 ./input_files/small/input.txt
if (( $(diff output.txt ./input_files/small/correct_output.txt) = 0 )); then echo OK; else echo ERROR! ; fi;
echo -e "\n\n################################################################################\nmedium"
time ./Release/IVCC01 ./input_files/medium/input.txt;
if [ 0 -eq $(diff output.txt ./input_files/medium/correct_output.txt) ]; then echo OK; else echo ERROR! ; fi;
echo -e "\n\n################################################################################\nlarge"
time ./Release/IVCC01 ./input_files/large/input.txt;
if [ 0 -eq $(diff output.txt ./input_files/large/correct_output.txt) ]; then echo OK; else echo ERROR! ; fi;
