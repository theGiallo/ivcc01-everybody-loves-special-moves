/*
 ============================================================================
 Name        : IVCC01.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */
#ifdef USE_OPENCL
#include "ocl_experiment.c"
#else
#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <syscall.h>
#include <string.h>

#ifdef DEBUG
#include <stdio.h>
#include <errno.h>
#endif

#define BUF_SIZE (1024*1024*2)
#define PERMS 0666
#define MAX_MOVES_NUM 100
#define MAX_MOVE_LENGTH 100

#ifdef __linux__
#define DINLINE inline
#endif
#ifdef __WIN32__
#define DINLINE _forceinline
#endif


int T_size;

/**
 * Read a file
 *
 * @param in_path string containing the path of the file to read
 * @param out_file pointer to the memory where the file read will be put
 * @param out_size size of the read file
 */
DINLINE void readFile(char* in_path, void** out_file, int* out_size)
{
	static int fd,i,r,iters,file_size = 0,last;
#ifdef DEBUG
	static int bread=0;
	printf("\n### read ###\n");
	printf("buf_size=%d\n", BUF_SIZE);
#endif
	static unsigned char *buf;
	__off_t off = 0;

	fd = open(in_path, O_RDONLY);	/// open file
#ifndef NO_SAFETY_CHECKS
	if (fd == -1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif

	off = lseek(fd,0,SEEK_END);	/// get length of the file
#ifndef NO_SAFETY_CHECKS
	if (off == (__off_t)-1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	file_size = off;
#ifdef DEBUG
	printf("file_size=%d\n", file_size);
#endif
	off = lseek(fd,0,SEEK_SET);
#ifndef NO_SAFETY_CHECKS
	if (off == (__off_t)-1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	buf = *out_file = malloc(file_size);	/// allocate file
#ifndef NO_SAFETY_CHECKS
	if (*out_file == NULL)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	last = file_size%BUF_SIZE;
	last = last?last:BUF_SIZE;
	iters = (file_size+BUF_SIZE-1)/BUF_SIZE;	/// calc num of reads relatively to BUF_SIZE
	iters--;
#ifdef DEBUG
	printf("last=%d\n", last);
	printf("iters=%d\n", iters);
#endif

	for(i = 0; i < iters; ++i)	/// read the file
	{
		r = read(fd, buf, BUF_SIZE);
#ifdef DEBUG
		bread+=r;
#endif
#ifndef NO_SAFETY_CHECKS
		if (r == -1)
		{
#ifdef DEBUG
			printf("error at line %d of file %s\n",__LINE__, __FILE__);
			perror(NULL);
#endif
			exit(__LINE__);
		}
#endif
		buf+=BUF_SIZE;
	}
	r = read(fd, buf, last);
#ifdef DEBUG
	bread+=r;
#endif
#ifndef NO_SAFETY_CHECKS
	if (r == -1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	buf+=last;
#ifdef DEBUG
	printf("bread = %d\n",bread);
#endif

	close(fd);	/// close file
	*out_size = file_size;
}

/**
 * Writes a file
 *
 * @param in_path string containing the path of the file to write
 * @param in_file pointer to the memory to write into the file
 * @param in_size size of the file
 */
DINLINE void writeFile(char* in_path, void* in_file, int in_size)
{
	static int fd,i,w,iters,last;
	static unsigned char *buf;
	buf = in_file;
#ifdef DEBUG
	int written = 0;
	printf("\n### write ###\n");
	printf("in_size=%d\n", in_size);
	printf("buf_size=%d\n", BUF_SIZE);
#endif

	fd = creat(in_path, PERMS);	/// open file
#ifndef NO_SAFETY_CHECKS
	if (fd == -1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	last = in_size%BUF_SIZE;
	last = last?last:BUF_SIZE;
	iters = (in_size+BUF_SIZE-1)/(int)BUF_SIZE;	/// calc num of reads relatively to BUF_SIZE
	iters--;
#ifdef DEBUG
	printf("last=%d\n", last);
	printf("iters=%d\n", iters);
#endif

	for(i = 0; i < iters; ++i)	/// write the file
	{
		w = write(fd, buf, BUF_SIZE);
#ifndef NO_SAFETY_CHECKS
		if (w == -1)
		{
#ifdef DEBUG
			printf("error at line %d of file %s\n",__LINE__, __FILE__);
			perror(NULL);
#endif
			exit(__LINE__);
		}
#endif
#ifdef DEBUG
		written += w;
#endif
		buf+=BUF_SIZE;
	}
	w = write(fd, buf, last);
#ifdef DEBUG
		written += w;
		printf("written=%d\n", written);
#endif
#ifndef NO_SAFETY_CHECKS
	if (w == -1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	buf+=last;

	close(fd);	/// close file

}

typedef unsigned char doubleckey_t;
enum ckeys
{
	D,d,U,u,L,l,R,r,P,p,K,k,b
};

typedef struct _move_st
{
	char* move_name;
	int name_length;
	char* move_keys;
	doubleckey_t compressed_keys[(MAX_MOVE_LENGTH+1)/2];
	int keys_length;
	short int pi[MAX_MOVES_NUM]; /// prefix function for KMP
}move_t;

/**
 * Read a move from a file in memory
 *
 * @param **in_out_file the memory pointer in which parse a move. It will be increased after parsing.
 * @param **out_move the parsed move will be allocated here
 */
void readMove(unsigned char** in_out_file, move_t** out_move)
{
	static int i;
	if ((*in_out_file)[0]=='\n')
	{
		*out_move = NULL;
		return;
	}
	*out_move = (move_t*)malloc(sizeof(move_t));
#ifndef NO_SAFETY_CHECKS
	if (*out_move == NULL)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	for (i=0; (*in_out_file)[i]!=':'; i++);
	(*out_move)->move_name = (*in_out_file);
	(*out_move)->name_length = i;

	(*in_out_file)+=i+1;
	for (i=0; (*in_out_file)[i]!='\n'; i++);
	(*out_move)->move_keys = (*in_out_file);
	(*out_move)->keys_length = i;
	(*in_out_file)+=i+1;
}

/**
 * Search for a pattern P in a string T
 *
 * @param *in_T string in which to search
 * @param *in_P pattern to search for into in_T
 */
void KMPMatcher(unsigned char *in_T, move_t *in_P, move_t **in_out_p_found)
{
	int i, m = in_P->keys_length, q = 0;
	short int *pi = in_P->pi;
	unsigned char *P = in_P->move_keys;
//	int to_null[MAX_MOVES_NUM];
//	short int to_null_count=0;
	for (i=0 ; in_T[i]!='\0' ; i++)
	{
#ifdef DEBUG
//			printf("q = %d\n",q);
#endif
		while (q>0 && P[q]!=in_T[i])
		{
			q = pi[q-1];
#ifdef DEBUG
			printf("\tq = %d i = %d\n",q,i);
#endif
			if (in_out_p_found[i-q]!=NULL)
			{
				i -= q;
				q = 0;
#ifdef DEBUG
			printf("\tq = %d i = %d\n",q,i);
#endif
			}
		}
		if (in_out_p_found[i]!=NULL)
//		{
			if (q==0)
			{
				i+=in_out_p_found[i]->keys_length;	/// only because we are given the moves cannot be contained in other moves
#ifdef DEBUG
				printf("\t\tq = %d i = %d\n",q,i);
#endif
			} //else
//			{
//				to_null[to_null_count] = i;
//				to_null_count++;
//			}
//		}
		if (P[q]==in_T[i])
		{
			q++;
		}
		if (q==m)
		{
#ifdef DEBUG
			printf("pattern found in %d; i = %d\n",i-m+1,i); /// <pattern found in i-m>
#endif
			in_out_p_found[i-m+1] = in_P;
//			for (q=0; q<m-1 ; q++)
//			{
//				in_out_p_found[i-q] = NULL;
//#ifdef DEBUG
//			printf("NULLing id %d\n",i-q);
//#endif
//			}
			q = 0;//pi[q-1];
		}
	}
}

/**
 * Computes KMP prefix function for a move
 *
 * @param *in_out_move the move to analyze
 */
void computePrefix(move_t* in_out_move)
{
	int k = 0, q,m=in_out_move->keys_length;
	char* P = in_out_move->move_keys;
	short int* pi = in_out_move->pi;
	pi[0] = 0;
#ifdef DEBUG
			printf("\tpi[0] = %d\n",pi[0]);
#endif
	for (q=1 ; q<m ; q++)
	{
		while (k>0 && P[k]!=P[q])
		{
			k = pi[k-1];
		}
		if (P[k]==P[q])
		{
			k++;
		}
		pi[q] = k;
#ifdef DEBUG
			printf("\tpi[%d] = %d\n",q,pi[q]);
#endif
	}
}

/**
 * Creates the output file to be written to fs
 *
 * @param *in_T the pressed keys text that has been analyzed
 * @param **in_p_found the array with the occurrences of found patterns(moves)
 * @param **out_output the blob of the output file (should be allocated externally as big as the input file)
 * @param *out_fsize the size of the output size
 */
void createOutput(unsigned char *in_T, move_t **in_p_found, unsigned char *out_output, int *out_fsize)
{
	int i,o,tmp;//,fd;
//	unsigned char buf[BUF_SIZE];
//	fd = creat("output.txt", PERMS);
//#ifndef NO_SAFETY_CHECKS
//	if (fd == -1)
//	{
//#ifdef DEBUG
//		printf("error at line %d of file %s\n",__LINE__, __FILE__);
//#endif
//		exit(__LINE__);
//	}
//#endif

	for (tmp=i=o=0 ; i<T_size; )	/// cycle on in_T
	{
		if (in_p_found[i]!=NULL) /// move found starting from index i
		{
			memcpy(&out_output[o],&in_T[tmp],i-tmp);
			o += i-tmp;
			if (o!=0)
			{
				out_output[o] = '\n';
				o++;
			}
			memcpy(&out_output[o],in_p_found[i]->move_name,in_p_found[i]->name_length);
//			for (tmp=0; tmp<in_p_found[i]->name_length; tmp++)
//			{
//				out_output[o] = in_p_found[i]->move_name[tmp];
//			}
			tmp = i;
			i+=in_p_found[tmp]->keys_length;
			o+=in_p_found[tmp]->name_length;
			out_output[o] = '\n';
			o++;
			tmp=i;
			i--;
		}// else	/// copy a key
//		{
//			out_output[o] = in_T[i];
			i++;
//			o++;
//		}
	}
	memcpy(&out_output[o],&in_T[tmp],i-tmp);
	o += i-tmp;
	out_output[o] = '\n';
	*out_fsize = o;
}

#ifdef DEBUG
void recreateInput(move_t **in_moves, unsigned char *in_keys, int in_k_size)
{
	int fd,i;
	fd = creat("recreated_input.txt",PERMS);

	for (i=0 ; i<MAX_MOVES_NUM; i++)
	{
		if (in_moves[i]==NULL)
		{
			break;
		}
		write(fd,in_moves[i]->move_name,in_moves[i]->name_length);
		write(fd,":",1);
		write(fd,in_moves[i]->move_keys,in_moves[i]->keys_length);
		write(fd,"\n",1);
	}
	write(fd,"\n",1);
	write(fd,in_keys,in_k_size);
	close(fd);
}
#endif

int main(void)
{
	unsigned char 	*input_file = NULL,
					*output_file,
					*p_file;	/// the string containing the pressed buttons to analyze
	move_t **p_found;	/// (long as p_file)at index i is NULL if no pattern has been found there or x if a pattern has been found there where x is the pointer to that move
	int 	inf_size = -1,
			i,
			num_moves,
//			T_size,
			outf_size;
	move_t *moves[MAX_MOVES_NUM];

	readFile("input.txt", (void**)&input_file, &inf_size);
	output_file = (unsigned char*) malloc(1.5*inf_size);	/// allocate memory for the output file
#ifndef NO_SAFETY_CHECKS
	if (output_file == NULL)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif

	p_file = input_file;
	i=-1;
	do
	{
		i++;
		readMove(&p_file, &moves[i]);
		if (moves[i]!=NULL)
			computePrefix(moves[i]);
	} while (moves[i]!=NULL);
	num_moves = i;

// the overhead of threads is too high for 10 moves
//	#pragma omp parallel for
//	for (i=0; i<num_moves;i++)
//	{
//		computePrefix(moves[i]);
//	}

	p_file++;	/// moves/keys separator; now p_file is pointing to keys
	T_size = inf_size-(p_file-input_file);
	p_found = (move_t**)calloc(T_size,sizeof(move_t*));
#ifdef DEBUG
	printf("%d moves read\nT_size=%d\n", num_moves,T_size);
#endif
#ifndef NO_SAFETY_CHECKS
	if (p_found == NULL)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif

#ifdef DEBUG
	writeFile("read_input.txt",input_file,inf_size);
	recreateInput(moves,p_file,T_size);
#endif

#ifdef USE_OMP
	#pragma omp parallel for
#endif
	for (i=0 ; i<num_moves ; i++)
	{
		KMPMatcher(p_file, moves[i], p_found);
	}
#ifdef USE_OMP
	#pragma omp barrier
#endif

	createOutput(p_file, p_found,output_file,&outf_size);
#ifdef DEBUG
	printf("\nprinting output.txt...\n");
#endif
	writeFile("output.txt",output_file,outf_size);

	return 0;
}
#endif // ifdef USE_OPENCL
