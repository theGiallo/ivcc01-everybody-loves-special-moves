// TODO REMOVE THIS
//#define USE_OPENCL
#ifdef USE_OPENCL
#ifndef OPENCL_MAIN
#define OPENCL_MAIN
//
// game of life
//
// on nVidia GeForce 8800 GT s must be power of 2
//
// this modification suggested by intel http://software.intel.com/en-us/articles/tips-and-tricks-for-kernel-development/#4
// seems not to change times of execution
//
// online reference manual for the various OpenCL functions:
// http://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/
// for understanding the meaning of the NULL parameters here and there ;-)
//

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>

#include <CL/cl.h>

#define BUF_SIZE (1024*1024*2)
#define PERMS 0666
#define MAX_MOVES_NUM 100
#define MAX_MOVE_LENGTH 100
/**
 * In the 1st cell we store the move length, so that +1.
 */
#define SINGLE_MOVE_BUF_SIZE (MAX_MOVE_LENGTH + 1)

#define TASK_BLOCK 128

/**
 * Contains the text to search into.
 */
#define BUF_T_RO 0
#define BUF_T_SIZE T_size

/**
 * Contains the patterns to search for.
 */
#define BUF_PS_RO 1
#define BUF_PS_SIZE (MAX_MOVES_NUM*SINGLE_MOVE_BUF_SIZE)

/**
 * Will be filled with the indexes where a pattern was found.
 * It's as long as BUF_T_RO
 */
#define BUF_PF_WO 2
#define BUF_PF_SIZE BUF_T_SIZE

///**
// * Contains the number of moves.
// */
//#define BUF_NUM_MOVES_RO 3
//#define BUF_NUM_MOVES_SIZE 1

#define NUM_BUFS 3

#define WORK_DIM 1

#define NO_MOVE_FOUND 0

const char *source =
//"#define TASK_BLOCK 128"
"__kernel void find(__global uchar *T, __global uchar *Ps, __global uchar *Pf, uchar n_P, int T_length) {"
"									       "
"  /* get matrix indices from work-item 1-D coordinates.         */	"
"  /* note we must NOT reverse the order:                        */	"
"  /* 1-D pattern has been specified matrix-style, not cartesian */	"
"  int i=0,TASK_BLOCK=128;"
"  int start_i = TASK_BLOCK*get_global_id(0);"
"  int end_i = min(start_i+TASK_BLOCK,T_length);"
"                           										"
"  uchar pid = 0;"
"  uchar ip = 0;"
"  uchar pl;"
"  for (i=start_i; i<end_i; i++){"
"    for (pid = 0; pid<n_P ; pid++){"
"      pl = Ps[pid*101];"
"      for (ip=0 ; ip<pl && (Ps[pid*101+1+ip]==T[i+ip]); ip++);"
"      if (ip==pl){"
"        Pf[i]=pid+1;"
"        pid=n_P;"
"        i+=pl-1;"
"        break;"
"      }"
"    }"
"  }"
"}";

/**
 * Prints the error returned from clBuildProgramError
 */
void printClBuildProgramError(struct _IO_FILE *where, cl_int e)
{
	switch (e)
	{
	case CL_INVALID_PROGRAM:
		printf("program is not a valid program object.\n");
		break;
	case CL_INVALID_VALUE:
		fprintf(where,"device_list is NULL and num_devices is greater than zero, or if device_list is not NULL and num_devices is zero.\nor\npfn_notify is NULL but user_data is not NULL.\n");
		break;
	case CL_INVALID_DEVICE:
		fprintf(where,"OpenCL devices listed in device_list are not in the list of devices associated with program.\n");
		break;
	case CL_INVALID_BINARY:
		fprintf(where,"program is created with clCreateProgramWithBinary and devices listed in device_list do not have a valid program binary loaded.\n");
		break;
	case CL_INVALID_BUILD_OPTIONS:
		fprintf(where,"the build options specified by options are invalid.\n");
		break;
	case CL_COMPILER_NOT_AVAILABLE:
		fprintf(where,"program is created with clCreateProgramWithSource and a compiler is not available i.e. CL_DEVICE_COMPILER_AVAILABLE specified in the table of OpenCL Device Queries for clGetDeviceInfo is set to CL_FALSE.\n");
		break;
	case CL_BUILD_PROGRAM_FAILURE:
		fprintf(where,"there is a failure to build the program executable. This error will be returned if clBuildProgram does not return until the build has completed.\n");
		break;
	case CL_INVALID_OPERATION:
		fprintf(where,"the build of a program executable for any of the devices listed in device_list by a previous call to clBuildProgram for program has not completed.\nor\nthere are kernel objects attached to program.\nor\nprogram was not created with clCreateProgramWithSource or clCreateProgramWithBinary.\n");
		break;
	case CL_OUT_OF_RESOURCES:
		fprintf(where,"there is a failure to allocate resources required by the OpenCL implementation on the device.\n");
		break;
	case CL_OUT_OF_HOST_MEMORY:
		fprintf(where,"there is a failure to allocate resources required by the OpenCL implementation on the host.\n");
		break;
	default:
		break;
	}
}

//////////////////////////////////////////////////////////////////////

#ifdef __linux__
#define DINLINE inline
#endif
#ifdef __WIN32__
#define DINLINE _forceinline
#endif

/**
 * Read a file
 *
 * @param in_path string containing the path of the file to read
 * @param out_file pointer to the memory where the file read will be put
 * @param out_size size of the read file
 */
DINLINE void readFile(char* in_path, void** out_file, int* out_size)
{
	static int fd,i,r,iters,file_size = 0,last;
#ifdef DEBUG
	static int bread=0;
	printf("\n### read ###\n");
	printf("buf_size=%d\n", BUF_SIZE);
#endif
	static unsigned char *buf;
	__off_t off = 0;

	fd = open(in_path, O_RDONLY);	/// open file
#ifndef NO_SAFETY_CHECKS
	if (fd == -1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif

	off = lseek(fd,0,SEEK_END);	/// get length of the file
#ifndef NO_SAFETY_CHECKS
	if (off == (__off_t)-1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	file_size = off;
#ifdef DEBUG
	printf("file_size=%d\n", file_size);
#endif
	off = lseek(fd,0,SEEK_SET);
#ifndef NO_SAFETY_CHECKS
	if (off == (__off_t)-1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	buf = *out_file = malloc(file_size);	/// allocate file
#ifndef NO_SAFETY_CHECKS
	if (*out_file == NULL)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	last = file_size%BUF_SIZE;
	last = last?last:BUF_SIZE;
	iters = file_size/BUF_SIZE + (last?1:0);	/// calc num of reads relatively to BUF_SIZE
	iters--;
#ifdef DEBUG
	printf("last=%d\n", last);
	printf("iters=%d\n", iters);
#endif

	for(i = 0; i < iters; ++i)	/// read the file
	{
		r = read(fd, buf, BUF_SIZE);
#ifdef DEBUG
		bread+=r;
#endif
#ifndef NO_SAFETY_CHECKS
		if (r == -1)
		{
#ifdef DEBUG
			printf("error at line %d of file %s\n",__LINE__, __FILE__);
			perror(NULL);
#endif
			exit(__LINE__);
		}
#endif
		buf+=BUF_SIZE;
	}
	r = read(fd, buf, last);
#ifdef DEBUG
	bread+=r;
#endif
#ifndef NO_SAFETY_CHECKS
	if (r == -1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	buf+=last;
#ifdef DEBUG
	printf("bread = %d\n",bread);
#endif

	close(fd);	/// close file
	*out_size = file_size;
}

/**
 * Writes a file
 *
 * @param in_path string containing the path of the file to write
 * @param in_file pointer to the memory to write into the file
 * @param in_size size of the file
 */
DINLINE void writeFile(char* in_path, void* in_file, int in_size)
{
	static int fd,i,w,iters,last;
	static unsigned char *buf;
	buf = in_file;
#ifdef DEBUG
	int written = 0;
	printf("\n### write ###\n");
	printf("in_size=%d\n", in_size);
	printf("buf_size=%d\n", BUF_SIZE);
#endif

	fd = creat(in_path, PERMS);	/// open file
#ifndef NO_SAFETY_CHECKS
	if (fd == -1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	last = in_size%BUF_SIZE;
	last = last?last:BUF_SIZE;
	iters = in_size/(int)BUF_SIZE + (last?1:0);	/// calc num of reads relatively to BUF_SIZE
	iters--;
#ifdef DEBUG
	printf("last=%d\n", last);
	printf("iters=%d\n", iters);
#endif

	for(i = 0; i < iters; ++i)	/// write the file
	{
		w = write(fd, buf, BUF_SIZE);
#ifndef NO_SAFETY_CHECKS
		if (w == -1)
		{
#ifdef DEBUG
			printf("error at line %d of file %s\n",__LINE__, __FILE__);
			perror(NULL);
#endif
			exit(__LINE__);
		}
#endif
#ifdef DEBUG
		written += w;
#endif
		buf+=BUF_SIZE;
	}
	w = write(fd, buf, last);
#ifdef DEBUG
		written += w;
		printf("written=%d\n", written);
#endif
#ifndef NO_SAFETY_CHECKS
	if (w == -1)
	{
#ifdef DEBUG
		printf("error at line %d of file %s\n",__LINE__, __FILE__);
		perror(NULL);
#endif
		exit(__LINE__);
	}
#endif
	buf+=last;

	close(fd);	/// close file

}

typedef struct _move_st
{
	cl_uchar* move_name; /// pointer to the in-memory file
	cl_uchar name_length;
	cl_uchar* move_keys; /// pointer to the P-array of cl
	cl_uchar keys_length;
}move_t;

/**
 * Read a move from a file in memory
 *
 * @param **in_out_file the memory pointer in which parse a move. It will be increased after parsing.
 * @param **out_move the parsed move. Must be allocated outside.
 */
void readMove(unsigned char** in_out_file, move_t *in_out_move)
{
	static cl_uchar i;
	if ((*in_out_file)[0]=='\n')
	{
		in_out_move->keys_length = 0;
		return;
	}
	for (i=0; (*in_out_file)[i]!=':'; i++);
	in_out_move->move_name = (*in_out_file);
	in_out_move->name_length = i;
	(*in_out_file)+=i+1; /// jump the ':'

	for (i=0; (*in_out_file)[i]!='\n'; i++);
	in_out_move->move_keys[0] = in_out_move->keys_length = i;
	memcpy(&in_out_move->move_keys[1], (*in_out_file),i);
	(*in_out_file)+=i+1; /// jump the '\n'
}


/**
 * Creates the output file to be written to fs
 *
 * @param *in_T the pressed keys text that has been analyzed
 * @param *moves the array with the moves
 * @param *in_p_found_ids the array with the indexes of found patterns(moves)
 * @param **out_output the blob of the output file (should be allocated externally as big as the input file)
 * @param *out_fsize the size of the output size
 */
void createOutput(unsigned char *in_T, move_t *moves, cl_uchar *in_p_found_ids, unsigned char *out_output, int *out_fsize)
{
	int i,o,tmp;

	for (tmp=i=o=0 ; in_T[i]!='\0'; )	/// cycle on in_T
	{
		if (in_p_found_ids[i]!=NO_MOVE_FOUND) /// move found starting from index i
		{
			memcpy(&out_output[o],&in_T[tmp],i-tmp);	/// write previous keys not in a move
			o += i-tmp;
			if (o!=0)
			{
				out_output[o] = '\n';
				o++;
			}
			memcpy(&out_output[o],moves[in_p_found_ids[i]-1].move_name,moves[in_p_found_ids[i]-1].name_length);
//			for (tmp=0; tmp<in_p_found[i]->name_length; tmp++)
//			{
//				out_output[o] = in_p_found[i]->move_name[tmp];
//			}
			tmp = i;
			i+=moves[in_p_found_ids[tmp]-1].keys_length;
			o+=moves[in_p_found_ids[tmp]-1].name_length;
			out_output[o] = '\n';
			o++;
			tmp = i;
			i--;
		}
		i++;
	}
	memcpy(&out_output[o],&in_T[tmp],i-tmp);
	o += i-tmp;
	out_output[o] = '\n';
	*out_fsize = o;
}

int main (int argc, const char * argv[])
{
	
	cl_platform_id platform_id;
	cl_device_id device;
	cl_uint n_platforms = 0, n_devices = 0;
	cl_context_properties properties[3];
	cl_int err = CL_SUCCESS;
	cl_context context;
	cl_command_queue queue;
	cl_program program;
	cl_kernel kernel;
	cl_mem w_bff[NUM_BUFS];

	// world[NUM_BUFS][...]
	cl_uchar *world[NUM_BUFS],num_moves;
	cl_int T_size,
		inf_size = -1,
		outf_size,
		i;

	unsigned char 	*input_file = NULL,
					*output_file,
					*p_file;	/// the string containing the pressed buttons to analyze
	move_t moves[MAX_MOVES_NUM];

	readFile("input.txt", (void**)&input_file, &inf_size);
	output_file = (unsigned char*) malloc(1.5*inf_size);	/// allocate memory for the output file

#ifdef DEBUG
	printf("\nparsing moves...\n");
#endif
	world[BUF_PS_RO] = (cl_uchar *)malloc( BUF_PS_SIZE * sizeof(cl_uchar));
	p_file = input_file;
	i=-1;
	do
	{
		i++;
		moves[i].move_keys = &world[BUF_PS_RO][i*SINGLE_MOVE_BUF_SIZE];
		readMove(&p_file, &moves[i]);
	} while (moves[i].keys_length!=0);
	num_moves = i;
	
	p_file++;	/// moves/keys separator; now p_file is pointing to keys
	T_size = inf_size-(p_file-input_file);

	world[BUF_T_RO] = p_file;//(cl_uchar *)malloc( BUF_T_SIZE * sizeof(cl_uchar));
	world[BUF_PF_WO] = (cl_uchar *)calloc( BUF_PF_SIZE, sizeof(cl_uchar)); /// will be filled with indexed +1 0 is a special value that means "none"

#ifdef DEBUG
	printf("\ngathering informations...\n");
#endif
	// get 1 platform
	if (clGetPlatformIDs(1,&platform_id,&n_platforms) != CL_SUCCESS) {
		fprintf(stderr,"error: no platform\n");
		return 1;
	};

	// on that platform, get 1 device of desired kind
	// (GPU, or CPU; we could also use both ;-)

//	if (clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_CPU, 1, &device, &n_devices) != CL_SUCCESS)
	if (clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &device, &n_devices) != CL_SUCCESS || n_devices<1)
	{
		fprintf(stderr,"error: no device\n");
		return 1;
	};


	// create a context for device.  properties list must end with 0
	properties[0]= CL_CONTEXT_PLATFORM;
	properties[1]= (cl_context_properties) platform_id;
	properties[2]= 0;

#ifdef DEBUG
	printf("\ncreating context...\n");
#endif
	context = clCreateContext(properties, 1, &device, NULL, NULL, &err);
//	context = clCreateContextFromType(properties, CL_DEVICE_TYPE_GPU, NULL, NULL, &err);
	if (err != CL_SUCCESS){
		fprintf(stderr,"error creating context\n");
		return 1;
	};

#ifdef DEBUG
	printf("\ncreating command queue...\n");
#endif
    // a queue for commands from host to device
	queue = clCreateCommandQueue(context, device, (cl_command_queue_properties)0, &err);
	if (err != CL_SUCCESS){
		fprintf(stderr,"error creating command queue\n");
		return 1;
	};
	
	// build the kernel program:
	// load kernel source, build kernel executable, create kernel object

#ifdef DEBUG
	printf("\ncreating program...\n");
#endif
	program = clCreateProgramWithSource(context, 1, (const char**)&source, NULL, &err);
	if (err != CL_SUCCESS){
		fprintf(stderr,"error creating kernel program\n");
		return 1;
	};

#ifdef DEBUG
	printf("\nbuilding program...\n");
#endif
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS){
		fprintf(stderr,"error building kernel program\n");
		printClBuildProgramError(stderr,err);
		return 1;
	};

#ifdef DEBUG
	printf("\ncreating kernel...\n");
#endif
	kernel = clCreateKernel(program, "find", &err);
	if (err != CL_SUCCESS){
		fprintf(stderr,"error creating kernel obj\n");
		return 1;
	};
	
	// specify a decomposition pattern (job --> work-items):
	// 1-D space with [n] work-items (one-to-one mapping to C[n]).
	// NOTE it is matrix style, not cartesian.
	// no group pattern at this time
	size_t global_pattern[] = {(BUF_T_SIZE+TASK_BLOCK-1)/TASK_BLOCK};
	size_t global_offset[] = {0};

	// create & pass parameters to kernel
	w_bff[BUF_T_RO] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, BUF_T_SIZE * sizeof(cl_uchar), world[BUF_T_RO], &err);
	if (err != CL_SUCCESS){fprintf(stderr,"error creating buffer at line %d\n",__LINE__);return 1;};
	w_bff[BUF_PS_RO] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, BUF_PS_SIZE * sizeof(cl_uchar), world[BUF_PS_RO], &err);
	if (err != CL_SUCCESS){fprintf(stderr,"error creating buffer at line %d\n",__LINE__);return 1;};
	w_bff[BUF_PF_WO] = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, BUF_PF_SIZE * sizeof(cl_uchar), world[BUF_PF_WO], &err);
	if (err != CL_SUCCESS){fprintf(stderr,"error creating buffer at line %d\n",__LINE__);return 1;};

	err = clSetKernelArg(kernel, BUF_T_RO, sizeof(w_bff[BUF_T_RO]), &w_bff[BUF_T_RO]);
	if (err != CL_SUCCESS){fprintf(stderr,"error(%d) passing parameters to kernel at line %d\n",err,__LINE__);return 1;};
	err = clSetKernelArg(kernel, BUF_PS_RO, sizeof(w_bff[BUF_PS_RO]), &w_bff[BUF_PS_RO]);
	if (err != CL_SUCCESS){fprintf(stderr,"error(%d) passing parameters to kernel at line %d\n",err,__LINE__);return 1;};
	err = clSetKernelArg(kernel, BUF_PF_WO, sizeof(w_bff[BUF_PF_WO]), &w_bff[BUF_PF_WO]);
	if (err != CL_SUCCESS){fprintf(stderr,"error(%d) passing parameters to kernel at line %d\n",err,__LINE__);return 1;};
	err = clSetKernelArg(kernel, NUM_BUFS, sizeof(num_moves), &num_moves);
	if (err != CL_SUCCESS){fprintf(stderr,"error(%d) passing parameters to kernel at line %d\n",err,__LINE__);return 1;};
	err = clSetKernelArg(kernel, NUM_BUFS+1, sizeof(T_size), &T_size);
	if (err != CL_SUCCESS){fprintf(stderr,"error(%d) passing parameters to kernel at line %d\n",err,__LINE__);return 1;};

#ifdef DEBUG
	printf("\nfinding moves...\n");
#endif
	// run the kernel.  "WORK_DIM" means "WORK_DIM-D decomposition"
	err = clEnqueueNDRangeKernel(queue, kernel, WORK_DIM, global_offset, global_pattern, NULL, 0, NULL, NULL);
	if (err != CL_SUCCESS){
		fprintf(stderr,"error(%d) running kernel\n",err);
		return 1;
	};

#ifdef DEBUG
	printf("\nreading cl output...\n");
#endif
	// retrieve results.
	// it is a **blocking** cmd (CL_TRUE) so we really wait for results
	err = clEnqueueReadBuffer(queue, w_bff[BUF_PF_WO], CL_TRUE, 0, BUF_PF_SIZE * sizeof(cl_uchar), world[BUF_PF_WO], 0, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		fprintf(stderr,"error getting results\n");
		return 1;
	};

#ifdef DEBUG
	printf("\ncreating output.txt...\n");
#endif
	createOutput(p_file, &moves[0], world[BUF_PF_WO],output_file,&outf_size);
#ifdef DEBUG
	printf("\nprinting output.txt...\n");
#endif
	writeFile("output.txt",output_file,outf_size);

	// clean up
	clReleaseMemObject(w_bff[BUF_T_RO]);
	clReleaseMemObject(w_bff[BUF_PS_RO]);
	clReleaseMemObject(w_bff[BUF_PF_WO]);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);
	
//	free(input_file);
//	free(output_file);
	free(world[BUF_PS_RO]);
	free(world[BUF_PF_WO]);

	return 0;
}
#endif // ifdef OPENCL_MAIN
#endif // ifdef USE_OPENCL
