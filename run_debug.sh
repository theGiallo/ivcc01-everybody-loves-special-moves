#!/bin/sh
echo "\n\n################################################################################\nsmall"
time ./Debug/IVCC01 ./input_files/small/input.txt
diff read_input.txt ./input_files/small/input.txt
echo "\n\n################################################################################\nmedium"
time ./Debug/IVCC01 ./input_files/medium/input.txt
diff read_input.txt ./input_files/medium/input.txt
echo "\n\n################################################################################\nlarge"
time ./Debug/IVCC01 ./input_files/large/input.txt
diff read_input.txt ./input_files/large/input.txt
